package function

// Status is an enum that can describe state of a function.
type Status string

const (
	// StatusReady indicates the function is available.
	StatusReady Status = "ready"

	// StatusInstalling indicates the function is currently in the process of getting deployed.
	StatusInstalling Status = "installing"

	// StatusError indicates error.
	StatusError Status = "error"
)

// Descriptor holds information about a specific function specification.
type Descriptor struct {
	Name     string        `json:"name,omitempty" yaml:"name,omitempty,omitempty"`
	Group    string        `json:"group,omitempty" yaml:"group,omitempty,omitempty"`
	Version  string        `json:"version,omitempty" yaml:"version,omitempty,omitempty"`
	Backend  BackendSpec   `json:"backend,omitempty" yaml:"backend,omitempty,omitempty"`
	Frontend *FrontendSpec `json:"frontend,omitempty" yaml:"frontend,omitempty,omitempty"`
	Status   *Status       `json:"status,omitempty" yaml:"status,omitempty,omitempty"`
	Meta     *Metadata     `json:"meta,omitempty" yaml:"meta,omitempty,omitempty"`
	Owner    *Account      `json:"owner,omitempty" yaml:"owner,omitempty"`
}

// FrontendSpec holds details about front end static content.
type FrontendSpec struct {
	Type   string `json:"type,omitempty" yaml:"type,omitempty"`
	Source string `json:"source,omitempty" yaml:"source,omitempty"`
}

// BackendSpec holds details about the endpoint the function can be reached at.
type BackendSpec struct {
	Endpoint string `json:"endpoint,omitempty" yaml:"endpoint,omitempty"`
	Image    string `json:"image,omitempty" yaml:"image,omitempty"`
	Type     string `json:"type,omitempty" yaml:"type,omitempty"`
	Port     *int32 `json:"port,omitempty" yaml:"port,omitempty"`
}

// Metadata holds details about the function.
type Metadata struct {
	Tags        []string `json:"tags,omitempty" yaml:"tags,omitempty"`
	Description string   `json:"description,omitempty" yaml:"description,omitempty"`
	Version     string   `json:"version,omitempty" yaml:"version,omitempty"`
	SourceURL   string   `json:"source_url,omitempty" yaml:"source_url,omitempty"`
}

// Account holds details on the owner of a function.
type Account struct {
	ID   string `json:"id,omitempty" yaml:"id,omitempty"`
	Name string `json:"name,omitempty" yaml:"name,omitempty"`
}
