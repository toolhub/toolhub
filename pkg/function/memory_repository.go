package function

import "fmt"

// MemoryRepository is a function descriptor reposiory that keeps everything in memory.
type MemoryRepository struct {
	store map[string]*Descriptor
}

// NewMemoryRepository creates and initializes a new memory repository.
func NewMemoryRepository() *MemoryRepository {
	return &MemoryRepository{
		store: make(map[string]*Descriptor),
	}
}

func hashFunction(group, name string) string {
	return fmt.Sprintf("%s/%s", group, name)
}

// Get retrieves the function from the local memory.
func (m *MemoryRepository) Get(group, name string) (*Descriptor, error) {
	if function, ok := m.store[hashFunction(group, name)]; ok {
		return function, nil
	}
	return nil, ErrNotFound
}

// Add adds the function to the store. If one already exists, it wil be replaced.
func (m *MemoryRepository) Add(function Descriptor) error {
	m.store[hashFunction(function.Group, function.Name)] = &function
	return nil
}

// List returns all functions inside the memory repository.
func (m *MemoryRepository) List() ([]*Descriptor, error) {
	results := make([]*Descriptor, 0, len(m.store))
	for _, value := range m.store {
		results = append(results, value)
	}
	return results, nil
}

// Clear removes all entries in the store.
func (m *MemoryRepository) Clear() error {
	m.store = make(map[string]*Descriptor)
	return nil
}
