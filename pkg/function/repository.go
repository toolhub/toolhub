package function

import "errors"

var (
	// ErrNotFound represents a generic not found error.
	ErrNotFound = errors.New("not found")
)

// ReadableRepository describes the ability to read functions from a repository.
type ReadableRepository interface {
	Get(group, name string) (*Descriptor, error)
	List() ([]*Descriptor, error)
}

// WritableRepository describes the ability to add functiosn to a repository.
type WritableRepository interface {
	Add(Descriptor) error
}

// Repository describes the ability to hold functions descriptors in a storage.
type Repository interface {
	ReadableRepository
	WritableRepository
}
