package probe

import (
	"context"
	"log"
	"time"

	"gitlab.com/toolhub/toolhub/pkg/cdp"
	"google.golang.org/grpc"
)

const maxTry = 20

func isFinal(result *cdp.ProbeResponse) bool {
	if result == nil {
		return false
	}
	for _, item := range result.Items {
		switch item.Status {
		case cdp.Content_ERROR, cdp.Content_READY:
			continue
		default:
			return false
		}
	}
	return true
}

type Service struct {
	URL string

	result *cdp.ProbeResponse
	active bool
}

// Result returns the current result.
func (c *Service) Result() *cdp.ProbeResponse {
	return c.result
}

// Probe dials the CDS server and keeps following up until results are in an end stage.
func (c *Service) Probe() {
	if !c.active {
		c.active = true
		go c.startProbe()
	}
}

func (c *Service) startProbe() {
	var connection *grpc.ClientConn
	tryIndex := 0
	defer func() {
		c.active = false
	}()
	for {
		var err error
		connection, err = grpc.Dial(c.URL, grpc.WithInsecure())
		if err == nil {
			break
		}
		log.Println(err)
		if tryIndex >= maxTry {
			log.Printf("giving up on %s", c.URL)
			return
		}
		tryIndex++
		time.Sleep(1 * time.Second)
	}
	tryIndex = 0
	for {
		client := cdp.NewContentProbeServiceClient(connection)
		response, err := client.Probe(context.Background(), &cdp.ProbeRequest{})
		if err != nil {
			log.Printf("CDS service %s failed: %s", c.URL, err)
			if tryIndex >= maxTry {
				log.Printf("giving up on %s", c.URL)
				return
			}
			tryIndex++
		}

		c.result = response
		if isFinal(response) {
			return
		}
		time.Sleep(1 * time.Second)
	}

}
