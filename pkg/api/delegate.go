package api

import (
	"context"
	"fmt"

	"gitlab.com/toolhub/toolhub/pkg/function"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

// DelegateRepo is a writable repository that saves functions via another API.
type DelegateRepo struct {
	url, token string
}

// NewDelegateRepo creates and initializes a new delegate repository.
func NewDelegateRepo(url, token string) *DelegateRepo {
	return &DelegateRepo{
		url:   url,
		token: token,
	}
}

func requestFromDescriptor(descriptor *function.Descriptor) *CreateFunctionRequest {
	request := &CreateFunctionRequest{
		Group: descriptor.Group,
		Name:  descriptor.Name,
	}

	if owner := descriptor.Owner; owner != nil {
		request.Owner = &Account{
			Id:   owner.ID,
			Name: owner.Name,
		}
	}

	if meta := descriptor.Meta; meta != nil {
		request.Metadata = &Metadata{
			Tags:        meta.Tags,
			Description: meta.Description,
			Version:     meta.Version,
			SourceUrl:   meta.SourceURL,
		}
	}

	request.Backend = &BackendSpec{
		Image: descriptor.Backend.Image,
		Type:  descriptor.Backend.Type,
	}
	if port := descriptor.Backend.Port; port != nil {
		request.Backend.Port = *port
	}

	if frontend := descriptor.Frontend; frontend != nil {
		request.Frontend = &FrontendSpec{
			Source: frontend.Source,
			Type:   frontend.Type,
		}
	}

	return request
}

// Add sends a request to create a new function to the delegated API.
func (d *DelegateRepo) Add(descriptor function.Descriptor) error {
	conn, err := grpc.Dial(d.url, grpc.WithInsecure())
	if err != nil {
		return fmt.Errorf("could not connect to API '%s': %s", d.url, err)
	}

	ctx := context.Background()
	if len(d.token) > 0 {
		ctx = metadata.AppendToOutgoingContext(ctx, "authorization", d.token)
	}

	client := NewFunctionServiceClient(conn)
	if _, err = client.CreateFunction(ctx, requestFromDescriptor(&descriptor)); err != nil {
		return err
	}

	return nil
}
