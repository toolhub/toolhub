package api

import (
	context "context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/toolhub/toolhub/pkg/api/probe"
	"gitlab.com/toolhub/toolhub/pkg/cdp"
	"gitlab.com/toolhub/toolhub/pkg/function"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
)

func backendStatus(info *function.Descriptor) Status_BackendStatus {
	if info.Status == nil {
		return Status_BACKEND_READY
	}
	switch *info.Status {
	case function.StatusReady:
		return Status_BACKEND_READY
	case function.StatusInstalling:
		return Status_BACKEND_INSTALLING
	case function.StatusError:
		fallthrough
	default:
		return Status_BACKEND_ERROR
	}
}

func frontendStatus(info *function.Descriptor, probeMap map[string]cdp.Content_Status) Status_FrontendStatus {
	if info.Frontend == nil {
		return Status_FRONTEND_MISSING
	}
	status, ok := probeMap[fmt.Sprintf("%s/%s", info.Group, info.Name)]
	if !ok {
		return Status_FRONTEND_MISSING
	}
	switch status {
	case cdp.Content_CLEANING_UP:
		return Status_FRONTEND_CLEANING_UP
	case cdp.Content_CLEAN_UP_QUEUE:
		return Status_FRONTEND_CLEAN_UP_QUEUE
	case cdp.Content_INSTALLATION_QUEUE:
		return Status_FRONTEND_INSTALLATION_QUEUE
	case cdp.Content_INSTALLING:
		return Status_FRONTEND_INSTALLING
	case cdp.Content_READY:
		return Status_FRONTEND_READY
	default:
		return Status_FRONTEND_ERROR
	}
}

func overallStatus(backend Status_BackendStatus, frontend Status_FrontendStatus) Status_Overall {
	var status Status_Overall
	var a, b uint8
	switch backend {
	case Status_BACKEND_ERROR:
		a = 1
	case Status_BACKEND_READY:
		a = 2
	}
	switch frontend {
	case Status_FRONTEND_ERROR:
		b = 1
	case Status_FRONTEND_READY, Status_FRONTEND_MISSING:
		b = 2
	}
	switch a + b {
	case 0:
		status = Status_WAITING
	case 1, 2:
		status = Status_ERROR
	case 3:
		status = Status_PARTIAL
	case 4:
		status = Status_READY
	}
	return status
}

func descriptorToFunction(info *function.Descriptor, probeMap map[string]cdp.Content_Status) *Function {
	backendStatus := backendStatus(info)
	frontEndStatus := frontendStatus(info, probeMap)
	function := &Function{
		Name:  info.Name,
		Group: info.Group,
		Status: &Status{
			FrontendStatus: frontEndStatus,
			BackendStatus:  backendStatus,
			Status:         overallStatus(backendStatus, frontEndStatus),
		},
	}

	if info.Meta != nil {
		function.Metadata = &Metadata{
			Tags:        info.Meta.Tags,
			Description: info.Meta.Description,
			Version:     info.Meta.Version,
			SourceUrl:   info.Meta.SourceURL,
		}
	}

	if info.Owner != nil {
		function.Owner = &Account{
			Id:   info.Owner.ID,
			Name: info.Owner.Name,
		}
	}

	return function
}

// FlexibleServer is an API server that uses a readable repository and an optional arbitrary writable
// repository.
type FlexibleServer struct {
	Reader    function.ReadableRepository
	Writer    function.WritableRepository
	Endpoints map[string]*AccessEndpoint

	// FIXME: use a proper authorization interface to delegate authorization out.
	PrimaryToken string

	probeService *probe.Service

	UnimplementedFunctionServiceServer
}

func NewFlexibleServer(reader function.ReadableRepository, cds *string) *FlexibleServer {
	server := &FlexibleServer{
		Reader: reader,
	}

	if cds != nil {
		server.probeService = &probe.Service{URL: *cds}
		go func() {
			for {
				server.probeService.Probe()
				time.Sleep(60 * time.Second)
			}
		}()
	}

	return server
}

func (f *FlexibleServer) probeMap() map[string]cdp.Content_Status {
	if f.probeService == nil {
		return nil
	}
	probeMap := map[string]cdp.Content_Status{}
	if result := f.probeService.Result(); result != nil {
		for _, item := range result.Items {
			probeMap[fmt.Sprintf("%s/%s", item.Group, item.Name)] = item.Status
		}
	}
	return probeMap
}

// ListFunctions returns all the records in the repository.
func (f *FlexibleServer) ListFunctions(context.Context, *ListFunctionRequest) (*ListFunctionResponse, error) {
	items, err := f.Reader.List()
	if err != nil {
		return nil, grpc.Errorf(codes.Unavailable, "storage service is not operational at the moment.")
	}
	result := make([]*Function, 0, len(items))
	for _, item := range items {
		result = append(result, descriptorToFunction(item, f.probeMap()))
	}
	return &ListFunctionResponse{
		Functions: result,
	}, nil
}

// GetFunction returns all details of a function.
func (f *FlexibleServer) GetFunction(ctx context.Context, req *GetFunctionRequest) (*GetFunctionResponse, error) {
	item, err := f.Reader.Get(req.Group, req.Name)
	if err != nil {
		if errors.Is(err, function.ErrNotFound) {
			return nil, grpc.Errorf(codes.NotFound, "function %s/%s does not exist.", req.Group, req.Name)
		}
		return nil, grpc.Errorf(codes.Unavailable, "storage service is not operational at the moment.")
	}
	return &GetFunctionResponse{Function: descriptorToFunction(item, f.probeMap())}, nil
}

// CreateFunction creates a function/tool.
func (f *FlexibleServer) CreateFunction(ctx context.Context, req *CreateFunctionRequest) (*CreateFunctionResponse, error) {
	if f.Writer == nil {
		return nil, grpc.Errorf(codes.Unavailable, "this repository does not support creating functions.")
	}
	if len(f.PrimaryToken) > 0 {
		m, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return nil, grpc.Errorf(codes.Unauthenticated, "this request must be authorized")
		}
		tokens := m.Get("authorization")
		if len(tokens) != 1 {
			return nil, grpc.Errorf(codes.InvalidArgument, "authorization metadata must only hold one token")
		}
		if tokens[0] != f.PrimaryToken {
			return nil, grpc.Errorf(codes.PermissionDenied, "invalid auth token")
		}
	}
	descriptor := function.Descriptor{
		Group: req.Group,
		Name:  req.Name,
	}
	if meta := req.Metadata; meta != nil {
		descriptor.Version = meta.Version
		descriptor.Meta = &function.Metadata{
			Tags:        meta.Tags,
			Description: meta.Description,
			SourceURL:   meta.SourceUrl,
		}
	}
	if owner := req.Owner; owner != nil {
		descriptor.Owner = &function.Account{ID: owner.Id, Name: owner.Name}
	}
	if backend := req.Backend; backend != nil {
		descriptor.Backend.Image = backend.Image
		descriptor.Backend.Type = backend.Type
		if backend.Port > 0 {
			descriptor.Backend.Port = &backend.Port
		}
	}
	if frontend := req.Frontend; frontend != nil {
		descriptor.Frontend = &function.FrontendSpec{
			Type:   frontend.Type,
			Source: frontend.Source,
		}
	}
	if err := f.Writer.Add(descriptor); err != nil {
		return nil, grpc.Errorf(codes.FailedPrecondition, "failed to create function: %s", err)
	}
	return &CreateFunctionResponse{}, nil
}

// ListEndpoints returns all advertised access endpoints.
func (f *FlexibleServer) ListEndpoints(context.Context, *ListEndpointRequest) (*ListEndpointResponse, error) {
	return &ListEndpointResponse{Endpoints: f.Endpoints}, nil
}
