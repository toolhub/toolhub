package watcher

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/toolhub/toolhub/pkg/function"
)

// Functions represents functions descriptors.
type Functions struct {
	Items []function.Descriptor `json:"items,omitempty" yaml:"items,omitempty"`
}

// FunctionsWatcher watches a config file and keeps a memory repository
// updated.
type FunctionsWatcher struct {
	repo    *function.MemoryRepository
	watcher *FileWatcher
	Errors  chan error
	Updates chan struct{}
}

// NewFunctionsWatcher returns a function watcher to keep a repository in sync with a config file.
// Any changes to the file triggers a sync.
func NewFunctionsWatcher(path string) FunctionsWatcher {
	return FunctionsWatcher{
		repo:    function.NewMemoryRepository(),
		watcher: NewFileWatcher(path),
		Errors:  make(chan error),
		Updates: make(chan struct{}),
	}
}

// Repo returns the internal function repository containing all the funcitons in the config file.
func (f *FunctionsWatcher) Repo() function.Repository {
	return f.repo
}

// Start begins watching the config file.
func (f *FunctionsWatcher) Start() error {
	if err := f.watcher.Start(); err != nil {
		return err
	}
	if err := f.load(); err != nil {
		return err
	}
	go func() {
		for {
			select {
			case <-f.watcher.Events:
				if err := f.load(); err != nil {
					f.Errors <- err
				} else {
					f.Updates <- struct{}{}
				}
			case err := <-f.watcher.Errors:
				f.Errors <- err
			}
		}
	}()
	return nil
}

func (f *FunctionsWatcher) load() error {
	file, err := os.Open(f.watcher.Path)
	if err != nil {
		return fmt.Errorf("failed to open config file: %s", err)
	}
	defer file.Close()
	var data Functions
	if err := json.NewDecoder(file).Decode(&data); err != nil {
		return fmt.Errorf("failed to load config file: %s", err)
	}
	if err := f.repo.Clear(); err != nil {
		return fmt.Errorf("failed to clear repository: %s", err)
	}
	for _, item := range data.Items {
		if err := f.repo.Add(item); err != nil {
			return fmt.Errorf("failed to add func %s/%s: %s", item.Group, item.Name, err)
		}
	}
	return nil
}
