// Package watcher contains watchers to sync a store from an external source.
package watcher

import (
	"github.com/fsnotify/fsnotify"
)

// Event holds different types of events.
type Event uint8

const (
	// EventUpdated is when config file is reloaded.
	EventUpdated Event = iota
)

// FileWatcher syncs entries from a config file with a function store.
type FileWatcher struct {
	Path   string
	Events chan Event
	Errors chan error
}

// NewFileWatcher makes a new file watcher.
func NewFileWatcher(path string) *FileWatcher {
	return &FileWatcher{
		Path:   path,
		Events: make(chan Event),
		Errors: make(chan error),
	}
}

// Start reads the file and updates the store or returns error.
// Afterwards, it'll start watching for changes in the file.
func (w *FileWatcher) Start() error {

	// Install watcher.
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return err
	}

	err = watcher.Add(w.Path)
	if err != nil {
		return err
	}

	go func() {
		defer watcher.Close()
		for {
			select {
			case event := <-watcher.Events:
				switch event.Op {
				case fsnotify.Remove:
					if err := watcher.Add(w.Path); err != nil {
						w.Errors <- err
					}
					fallthrough
				case fsnotify.Create, fsnotify.Write:
					w.Events <- EventUpdated
				}
			case err := <-watcher.Errors:
				w.Errors <- err
			}
		}
	}()

	return nil
}
