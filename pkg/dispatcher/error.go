package dispatcher

// HTTPError describes an HTTP error.
type HTTPError struct {
	Status  int
	Message string `json:"message"`
}

func (h HTTPError) Error() string {
	return h.Message
}
