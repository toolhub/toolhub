// Package dispatcher contains servers and types to support routing and dispatching
// functions to their endpoints.
package dispatcher

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"gitlab.com/toolhub/toolhub/pkg/function"
)

// Server is a dispatcher service server.
type Server struct {
	Repo   function.ReadableRepository
	Client *http.Client
}

// NewServer creates a new dispatcher server.
// If client is nil, the default client will be used.
func NewServer(repo function.Repository) *Server {
	return &Server{
		Repo:   repo,
		Client: &http.Client{Timeout: 60 * time.Second},
	}
}

func (s *Server) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	if err := s.handleRequest(writer, request); err != nil {
		switch httpError := err.(type) {
		case HTTPError:
			writer.WriteHeader(httpError.Status)
			encoder := json.NewEncoder(writer)
			encoder.Encode(err)
		default:
			writer.WriteHeader(500)
			encoder := json.NewEncoder(writer)
			encoder.Encode(err)
		}
	}
}

func (s *Server) handleRequest(writer http.ResponseWriter, request *http.Request) error {
	if request.Method != http.MethodPost {
		return HTTPError{Status: http.StatusMethodNotAllowed, Message: "method not allowed, use POST."}
	}
	if request.Body != nil {
		defer request.Body.Close()
	}
	parts := strings.SplitN(request.URL.Path[1:], "/", 3)
	if len(parts) < 2 || (len(parts) == 3 && parts[2] != "") {
		http.NotFound(writer, request)
		return nil
	}
	group := parts[0]
	name := parts[1]
	f, err := s.Repo.Get(group, name)
	if err != nil {
		if errors.Is(err, function.ErrNotFound) {
			return HTTPError{Status: http.StatusNotFound, Message: fmt.Sprintf("function %s/%s does not exist.", group, name)}
		}
		return err
	}
	// TODO: Definitely measure the performance and benchmark this.
	proxyRequest, err := http.NewRequest("POST", f.Backend.Endpoint, request.Body)
	// Redirect custom headers.
	for key, values := range request.Header {
		if strings.HasPrefix(key, "X-") {
			for _, value := range values {
				proxyRequest.Header.Add(key, value)
			}
		}
	}
	if value := request.Header.Get("Content-Type"); len(value) > 0 {
		proxyRequest.Header.Add("Content-Type", value)
	}
	if value := request.Header.Get("Content-Length"); len(value) > 0 {
		proxyRequest.Header.Add("Content-Length", value)
	}
	if err != nil {
		return HTTPError{Status: http.StatusBadGateway, Message: fmt.Sprintf("Cannot evaluate function %s/%s.", group, name)}
	}
	response, err := s.Client.Do(proxyRequest)
	if err != nil {
		return HTTPError{Status: http.StatusBadGateway, Message: fmt.Sprintf("Cannot evaluate function %s/%s.", group, name)}
	}
	for key, values := range response.Header {
		for _, value := range values {
			writer.Header().Add(key, value)
		}
	}
	writer.WriteHeader(response.StatusCode)
	io.Copy(writer, response.Body)
	return nil
}
