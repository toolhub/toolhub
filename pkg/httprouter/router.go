// Package httprouter provides a special router that allows for changing handlers during runtime.
package httprouter

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"

	"gitlab.com/toolhub/toolhub/pkg/api"
	"gitlab.com/toolhub/toolhub/pkg/endpoint"
	"gitlab.com/toolhub/toolhub/pkg/watcher"
)

// Target is the acccess endpoint target.
type Target uint8

type httpLocationSelector func(*api.AccessEndpoint) string

func selectorFor(target Target) httpLocationSelector {
	switch target {
	case CDS:
		return func(e *api.AccessEndpoint) string {
			return e.Cds
		}
	case Dashboard:
		return func(e *api.AccessEndpoint) string {
			return e.Cds
		}
	case API:
		return func(e *api.AccessEndpoint) string {
			if e.Api == nil {
				return ""
			}
			return e.Api.Gateway
		}
	case Dispatcher:
		return func(e *api.AccessEndpoint) string {
			if e.Dispatcher == nil {
				return ""
			}
			return e.Dispatcher.Http
		}
	default:
		return func(*api.AccessEndpoint) string {
			return ""
		}
	}
}

const (
	// CDS is the content delivery service target.
	CDS Target = iota

	// Dispatcher is the HTTP dispatcher target.
	Dispatcher

	// API is the Gateway API target.
	API

	// Dashboard is the dashboard target.
	Dashboard
)

// Initializer describes the ability to initialize a variable multiplexer.
type Initializer interface {
	Initialize(*Mux) error
}

// InitializerFunc is a shortcut for defining simple initializer functions.
type InitializerFunc func(*Mux) error

// Initialize initializes a variable multiplexer.
func (i InitializerFunc) Initialize(m *Mux) error {
	return i(m)
}

// Options holds all options for the multiplexer.
type Options struct {
	Initializer Initializer
}

// Option describes the ability to set an option.
type Option interface {
	Apply(*Options)
}

// OptionFunc is a shortcut for defining simple option functions.
type OptionFunc func(*Options)

// Apply updates an option.
func (o OptionFunc) Apply(opts *Options) {
	o(opts)
}

// WithHandler uses an initial http handler. This can be changed with mux.Use(handler) method.
func WithHandler(handler http.Handler) Option {
	return OptionFunc(func(opts *Options) {
		opts.Initializer = InitializerFunc(func(m *Mux) error {
			m.Use(handler)
			return nil
		})
	})
}

func createRoutes(
	endpoints map[string]*api.AccessEndpoint,
	handler http.Handler, selector httpLocationSelector) (*http.ServeMux, []string) {
	routeSet := map[string]struct{}{}
	newRouter := http.NewServeMux()

	for _, endpoint := range endpoints {
		location := selector(endpoint)
		// if location is only a hostname or left empty, root is assumed.
		if i := strings.Index(location, "/"); i != -1 {
			route := location[i:]
			if !strings.HasSuffix(route, "/") {
				route = route + "/"
			}
			routeSet[route] = struct{}{}
		} else {
			routeSet["/"] = struct{}{}
		}
	}

	routes := make([]string, 0, len(routeSet))
	for route := range routeSet {
		routes = append(routes, route)
		if route == "/" {
			newRouter.Handle(route, handler)
		} else {
			newRouter.Handle(route, http.StripPrefix(strings.TrimSuffix(route, "/"), handler))
		}
	}
	return newRouter, routes
}

type configWatcher struct {
	Handler  http.Handler
	Path     string
	Selector httpLocationSelector
	Errors   chan<- error
	Routes   chan<- []string
	Mux      *Mux
}

func (c *configWatcher) Start() error {
	watcher := watcher.NewFileWatcher(c.Path)
	if err := watcher.Start(); err != nil {
		return err
	}
	routes, err := c.Reload()
	if err != nil {
		return err
	}
	c.Routes <- routes
	go func() {
		for {
			select {
			case <-watcher.Events:
				if routes, err := c.Reload(); err != nil {
					c.Errors <- fmt.Errorf("failed to process access endpoints file: %s", err)
				} else {
					c.Routes <- routes
				}
			case err := <-watcher.Errors:
				c.Errors <- err
			}
		}
	}()
	return nil
}

func (c *configWatcher) Reload() ([]string, error) {
	var config endpoint.Config

	file, err := os.Open(c.Path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	if err := json.NewDecoder(file).Decode(&config); err != nil {
		return nil, fmt.Errorf("failed to JSON decode '%s': %s", c.Path, err)
	}

	newMux, routes := createRoutes(config.Endpoints, c.Handler, c.Selector)
	c.Mux.Use(newMux)

	return routes, nil
}

// WithConfigWatching watches an access endpoints config file to define routes.
func WithConfigWatching(handler http.Handler, path string, target Target, errors chan<- error, routes chan<- []string) Option {
	return OptionFunc(func(opts *Options) {
		opts.Initializer = InitializerFunc(func(m *Mux) error {
			manager := &configWatcher{
				Handler:  handler,
				Path:     path,
				Selector: selectorFor(target),
				Errors:   errors,
				Routes:   routes,
				Mux:      m,
			}
			if err := manager.Start(); err != nil {
				return fmt.Errorf("failed to start file watcher for access endpoints file at '%s': %s", path, err)
			}
			return nil
		})
	})
}

// WithAccessEndpoints creates appropriate routes based on access endpoints table.
func WithAccessEndpoints(handler http.Handler, endpoints map[string]*api.AccessEndpoint, target Target) Option {
	return OptionFunc(func(opts *Options) {
		opts.Initializer = InitializerFunc(func(m *Mux) error {
			mux, _ := createRoutes(endpoints, handler, selectorFor(target))
			m.Use(mux)
			return nil
		})
	})
}

// Mux is a simple lightweight wrapper that allows underlying handler to change at runtime.
//
// example:
//    mux := httprouter.Mux{Handler: http.DefaultServeMux}
//    mux.Use(http.NewServeMux())
type Mux struct {
	Handler http.Handler
}

// New returns a new httprouter multiplexer.
func New(opts ...Option) (*Mux, error) {
	options := Options{}
	for _, option := range opts {
		option.Apply(&options)
	}
	mux := &Mux{Handler: http.NotFoundHandler()}
	if options.Initializer != nil {
		if err := options.Initializer.Initialize(mux); err != nil {
			return nil, fmt.Errorf("initializer failed: %s", err)
		}
	}
	return mux, nil
}

// Use changes the underlying serve mux.
func (m *Mux) Use(handler http.Handler) {
	m.Handler = handler
}

func (m *Mux) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	m.Handler.ServeHTTP(writer, request)
}
