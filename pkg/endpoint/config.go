package endpoint

import "gitlab.com/toolhub/toolhub/pkg/api"

// Config holds an access endpoints config structure.
type Config struct {
	Endpoints map[string]*api.AccessEndpoint `json:"endpoints,omitempty" yaml:"endpoints,omitempty"`
}
