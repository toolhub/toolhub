package endpoint_test

import (
	"testing"

	"gitlab.com/toolhub/toolhub/pkg/api"
	"gitlab.com/toolhub/toolhub/pkg/endpoint"
)

type SelectTestCase struct {
	Name          string
	Input         map[string]*api.AccessEndpoint
	API           string
	GatewayAPI    string
	DispatcherURL string
}

func TestSelect(t *testing.T) {
	general := map[string]*api.AccessEndpoint{
		"toolstudio.io": {
			Insecure: false,
			Api: &api.RPCEndpoint{
				Gateway: "api.toolstudio.io/web",
				Grpc:    "api.toolstudio.io",
			},
			Dispatcher: &api.RPCEndpoint{
				Http: "dispatcher.toolstudio.io",
			},
		},
		"*": {
			Insecure: true,
			Api: &api.RPCEndpoint{
				Gateway: "localhost:5000/web",
				Grpc:    "localhost:4000",
			},
			Dispatcher: &api.RPCEndpoint{
				Http: "localhost:5000/dispatcher",
			},
		},
	}
	cases := []SelectTestCase{
		{
			Name:          "smoke",
			Input:         general,
			API:           "api.toolstudio.io",
			GatewayAPI:    "api.toolstudio.io/web",
			DispatcherURL: "https://dispatcher.toolstudio.io",
		},
		{
			Name:          "wildcard",
			Input:         general,
			API:           "random.stuff",
			GatewayAPI:    "some.other.stuff/a",
			DispatcherURL: "http://localhost:5000/dispatcher",
		},
		{
			Name: "no-match",
			Input: map[string]*api.AccessEndpoint{
				"toolstudio.io": general["toolstudio.io"],
			},
			API:        "mismatch",
			GatewayAPI: "mismatch",
		},
		{
			Name: "localhost",
			Input: map[string]*api.AccessEndpoint{
				"localhost:4000": {
					Api: &api.RPCEndpoint{
						Gateway: "/web",
						Grpc:    "localhost:3000",
					},
					Dispatcher: &api.RPCEndpoint{
						Http: "/dispatcher",
					},
					Insecure: true,
				},
			},
			API:           "localhost:3000",
			GatewayAPI:    "localhost:4000/web",
			DispatcherURL: "http://localhost:4000/dispatcher",
		},
	}

	for _, testCase := range cases {
		_, grpcResult, _ := endpoint.Select(testCase.Input, endpoint.MatchAPI(testCase.API, endpoint.GRPC))
		hostname, gatewayResult, ok := endpoint.Select(testCase.Input, endpoint.MatchAPI(testCase.GatewayAPI, endpoint.Gateway))
		if grpcResult != gatewayResult {
			t.Errorf(
				"test case %s failed because api and gateway queries return different values. api: %v, gateway: %v",
				testCase.Name, grpcResult, gatewayResult,
			)
		}
		if testCase.DispatcherURL == "" {
			if ok {
				t.Errorf("test case %s failed because expected no result but got ok: %v", testCase.Name, gatewayResult)
			}
		} else {
			if !ok {
				t.Errorf("test case %s failed because it didn't find any match.", testCase.Name)
			} else {
				url := endpoint.CreateHTTP(hostname, gatewayResult.Dispatcher.Http, gatewayResult.Insecure)
				if url != testCase.DispatcherURL {
					t.Errorf("test case %s failed becuase of wrong result. expected %s got %s", testCase.Name, testCase.DispatcherURL, url)
				}
			}
		}
	}
}
