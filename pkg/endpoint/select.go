package endpoint

import (
	"net/url"
	"strings"

	"gitlab.com/toolhub/toolhub/pkg/api"
)

// Type describes the type of an endpoint.
type Type uint8

const (
	// GRPC endpoint describes a GRPC server.
	GRPC Type = iota
	// Gateway endpoint is the gRPC-Web gateway.
	Gateway
	// HTTP endpoint is a REST API. This could be gRPC Gateway, not to be confused with gRPC-Web.
	HTTP
)

// CreateHTTP constructs an HTTP endpoint based on base, path and security scheme.
func CreateHTTP(base, path string, insecure bool) string {
	u := url.URL{Scheme: "https", Host: base}
	if strings.HasPrefix(path, "/") {
		u.Path = path
	} else {
		index := strings.Index(path, "/")
		if index != -1 {
			u.Host = path[:index]
			u.Path = path[index+1:]
		} else {
			u.Host = path
		}
	}
	if insecure {
		u.Scheme = "http"
	}
	return u.String()
}

// Query describes the ability to check whether an access point is a match.
type Query interface {
	Match(string, *api.AccessEndpoint) bool
}

type queryFuncWrapper struct {
	matcher func(string, *api.AccessEndpoint) bool
}

func (q queryFuncWrapper) Match(hostname string, endpoint *api.AccessEndpoint) bool {
	return q.matcher(hostname, endpoint)
}

func matchAddress(query, hostname string, endpoint *api.AccessEndpoint, rpc *api.RPCEndpoint, endpointType Type) bool {
	switch endpointType {
	case GRPC:
		return query == rpc.Grpc
	case HTTP:
		return query == CreateHTTP(hostname, rpc.Http, endpoint.Insecure)
	case Gateway:
		return query == CreateHTTP(hostname, rpc.Gateway, endpoint.Insecure)
	default:
		return false
	}
}

// MatchAPI creates a Query that matches access endpoints based on their API.
func MatchAPI(address string, endpointType Type) Query {
	return queryFuncWrapper{
		func(hostname string, e *api.AccessEndpoint) bool {
			return matchAddress(address, hostname, e, e.Api, endpointType)
		},
	}
}

// Select uses the specified query to find an access endpoint or return null.
func Select(endpoints map[string]*api.AccessEndpoint, q Query) (string, *api.AccessEndpoint, bool) {
	for hostname, endpoint := range endpoints {
		if q.Match(hostname, endpoint) {
			return hostname, endpoint, true
		}
	}
	if defaultEndpoint, ok := endpoints["*"]; ok {
		return "", defaultEndpoint, true
	}
	return "", nil, false
}
