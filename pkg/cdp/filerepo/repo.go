// Package filerepo includes tools to install blobs at a local repository.
package filerepo

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"gitlab.com/toolhub/toolhub/pkg/cdp/driver"
)

// Manager takes charge of managing files in a local directory.
type Manager struct {
	RootDir string
}

// New initializes a manager and returns if successful.
func New(path string) (*Manager, error) {
	if err := os.MkdirAll(path, os.ModePerm); err != nil {
		return nil, err
	}
	return &Manager{RootDir: path}, nil
}

// Install adds the files to the local file repository.
func (m *Manager) Install(name, group string, blobs []driver.Blob) error {
	destination := filepath.Join(m.RootDir, group, name)
	if err := os.RemoveAll(destination); err != nil {
		return err
	}

	if err := os.MkdirAll(destination, os.ModePerm); err != nil {
		return err
	}

	for _, blob := range blobs {
		dirPath := filepath.Join(destination, blob.Prefix)
		if err := os.MkdirAll(dirPath, os.ModePerm); err != nil {
			return err
		}
		path := filepath.Join(dirPath, blob.Name)
		file, err := os.Create(path)
		if err != nil {
			return fmt.Errorf("could not create %s: %s", blob.Name, err)
		}
		_, err = io.Copy(file, bytes.NewReader(blob.Data))
		if err != nil {
			file.Close()
			return fmt.Errorf("could not write to %s: %s", blob.Name, err)
		}
		file.Close()
	}

	return nil
}
