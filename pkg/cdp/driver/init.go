package driver

import (
	"fmt"

	"gitlab.com/toolhub/toolhub/pkg/cdp/source"
)

// Blob represents a file.
type Blob struct {
	Name   string
	Prefix string
	Data   []byte
}

func (b Blob) String() string {
	return fmt.Sprintf("[%s F %s - %d bytes]", b.Prefix, b.Name, len(b.Data))
}

// Interface describes the ability of a driver to retrieve content.
type Interface interface {
	Retrieve(source.Descriptor) ([]Blob, error)
}

// Retrieve automatically chooses an appropriate driver and retrieves the content.
func Retrieve(s source.Descriptor) ([]Blob, error) {
	var driver Interface

	switch s.Type {
	case "github":
		driver = GitHub{}
	case "gitlab":
		driver = GitLab{}
	default:
		return nil, fmt.Errorf("unknown source type '%s'", s.Type)
	}

	return driver.Retrieve(s)
}
