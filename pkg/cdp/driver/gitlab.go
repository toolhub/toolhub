// Package driver includes some file retrieval drivers.
package driver

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"net/http"
	"path/filepath"
	"strings"

	"gitlab.com/toolhub/toolhub/pkg/cdp/source"
)

// GitLab driver retrieves files from a GitLab repository.
type GitLab struct{}

// Retrieve returns a list of related blobs from a github repository.
func (g GitLab) Retrieve(s source.Descriptor) ([]Blob, error) {
	ref := "main"
	if len(s.Params["ref"]) > 0 {
		ref = s.Params["ref"]
	}
	repo := s.Params["repo"]
	prefix := s.Params["prefix"]
	if len(repo) == 0 {
		return nil, fmt.Errorf("missing repo param")
	}
	parts := strings.Split(repo, "/")
	if len(parts) < 2 {
		return nil, fmt.Errorf("invalid repo '%s'", repo)
	}
	repoName := parts[len(parts)-1]
	url := fmt.Sprintf("https://gitlab.com/%s/-/archive/%s/%s-%s.tar.gz", repo, ref, repoName, ref)
	response, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("failed to get the resource: %s", err)
	}

	if response.StatusCode != 200 {
		return nil, fmt.Errorf("got status: %d", response.StatusCode)
	}

	if response.Body != nil {
		defer response.Body.Close()
	}

	decompressor, err := gzip.NewReader(response.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read gzip: %s", err)
	}

	reader := tar.NewReader(decompressor)
	result := []Blob{}

	for {
		header, err := reader.Next()
		switch {
		case err == io.EOF:
			return result, nil
		case err != nil:
			return nil, fmt.Errorf("failed to read tarball: %s", err)
		case header == nil:
			continue
		case header.Typeflag != tar.TypeReg:
			continue
		}

		buffer := &bytes.Buffer{}
		_, err = io.Copy(buffer, reader)
		if err != nil {
			return nil, fmt.Errorf("failed to read from file %s: %s", header.Name, err)
		}
		dir, filename := filepath.Split(header.Name)
		dir = dir[strings.Index(dir, "/")+1:]
		if !strings.HasPrefix(dir, prefix) {
			fmt.Printf("skipping %s\n", dir)
			continue
		}
		fmt.Printf("keeping %s\n", header.Name)
		result = append(result, Blob{
			Name:   filename,
			Prefix: strings.TrimPrefix(dir, prefix),
			Data:   buffer.Bytes(),
		})
	}
}
