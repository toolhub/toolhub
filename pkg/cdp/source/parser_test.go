package source_test

import (
	"testing"

	"gitlab.com/toolhub/toolhub/pkg/cdp/source"
)

func TestParse(t *testing.T) {
	tests := []struct {
		Name        string
		Input       string
		Expectation source.Descriptor
	}{
		{
			Input: "git:repo='github.com/toolhub/something.git'",
			Name:  "smoke",
			Expectation: source.Descriptor{
				Type: "git",
				Params: map[string]string{
					"repo": "github.com/toolhub/something.git",
				},
			},
		},
		{
			Input: "git:repo='github.com/toolhub/something.git',path='function1/html'",
			Name:  "multi-param",
			Expectation: source.Descriptor{
				Type: "git",
				Params: map[string]string{
					"repo": "github.com/toolhub/something.git",
					"path": "function1/html",
				},
			},
		},
	}

	for _, test := range tests {
		result, err := source.Parse(test.Input)
		if err != nil {
			t.Errorf("test %s got parser error: %s", test.Name, err)
			continue
		}
		if !result.Equal(test.Expectation) {
			t.Errorf("failed %s, expected %v but got %v", test.Name, test.Expectation, result)
		}
	}
}
