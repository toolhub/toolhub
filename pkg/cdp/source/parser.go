package source

import (
	"errors"
	"regexp"
	"strings"
)

var matcher = regexp.MustCompile(`(.*):((?:[\w\d]*='[\w-/.]*',)*(?:[\w\d]*='[\w-/.]*'))`)

// Parse creates a Descriptor from the input string if possible.
func Parse(input string) (Descriptor, error) {
	parts := matcher.FindStringSubmatch(input)
	if parts == nil {
		return Descriptor{}, errors.New("invalid format")
	}
	params := map[string]string{}
	for _, part := range strings.Split(parts[2], ",") {
		subParts := strings.Split(part, "=")
		params[subParts[0]] = strings.Trim(subParts[1], "'")
	}
	return Descriptor{
		Type:   parts[1],
		Params: params,
	}, nil
}
