// Package source provides types and parsers to understand a content source.
package source

import "reflect"

// Descriptor holds information about a source.
type Descriptor struct {
	Type   string
	Params map[string]string
}

// Equal returns whether or not a descriptor is equal to another.
func (d Descriptor) Equal(other Descriptor) bool {
	return d.Type == other.Type && reflect.DeepEqual(d.Params, other.Params)
}
