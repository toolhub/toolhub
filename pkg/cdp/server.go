package cdp

import (
	context "context"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/toolhub/toolhub/pkg/cdp/driver"
	"gitlab.com/toolhub/toolhub/pkg/cdp/filerepo"
	"gitlab.com/toolhub/toolhub/pkg/cdp/source"
	"gitlab.com/toolhub/toolhub/pkg/function"
)

type installationStatus uint8
type jobAction uint8

const (
	statusInstalled installationStatus = iota
	statusInstallation
	statusCleanup
	statusError
)

const (
	actionRemove = iota
	actionInstall
)

const workerCount = 2

type installation struct {
	name       string
	group      string
	status     installationStatus
	inProgress bool
	descriptor source.Descriptor
}

func (i installation) Equal(other installation) bool {
	return i.name == other.name &&
		i.group == other.group &&
		i.status == other.status &&
		i.descriptor.Equal(other.descriptor)
}

type job struct {
	action jobAction
	installation
}

type jobError struct {
	job
	err error
}

// Server is a content delivery server that watches a config and installs
// content and is able to handle probe requests.
type Server struct {
	// ContentPath is the directory that will be used to save content.
	ContentPath string

	// Repo is the source of all functions that need to be installed.
	Repo function.Repository

	storage       *filerepo.Manager
	installations map[string]installation

	removeCh chan string       // request to remove.
	addCh    chan installation // request to add.
	updateCh chan installation // request to update.

	workerDoneCh chan job      // worker completion channel.
	workerErrCh  chan jobError // worker error channel.
	workerJobCh  chan job      // worker remove channel.
}

// New creates and initializes a service.
func New(repo function.Repository, contentPath string) (*Server, error) {
	storage, err := filerepo.New(contentPath)
	if err != nil {
		return nil, fmt.Errorf("could not init file repo: %s", err)
	}
	svc := &Server{
		Repo:          repo,
		ContentPath:   contentPath,
		storage:       storage,
		installations: map[string]installation{},

		removeCh: make(chan string),
		addCh:    make(chan installation),
		updateCh: make(chan installation),

		workerDoneCh: make(chan job),
		workerErrCh:  make(chan jobError),
		workerJobCh:  make(chan job),
	}
	go svc.startControlLoop()
	for i := 0; i < workerCount; i++ {
		go svc.startWorker(svc.workerJobCh, svc.workerDoneCh, svc.workerErrCh)
	}
	return svc, nil
}

func (c *Server) startControlLoop() {
	busyCount := 0
	for {
		select {
		case key := <-c.removeCh:
			existing, ok := c.installations[key]
			if !ok {
				break
			}
			existing.status = statusCleanup
			existing.descriptor = source.Descriptor{Params: map[string]string{}}
			c.installations[key] = existing
		case item := <-c.updateCh:
			key := fmt.Sprintf("%s/%s", item.group, item.name)
			existing := c.installations[key]
			existing.status = statusInstallation
			existing.descriptor = item.descriptor
			c.installations[key] = existing
		case item := <-c.addCh:
			key := fmt.Sprintf("%s/%s", item.group, item.name)
			item.status = statusInstallation
			c.installations[key] = item
		case jobErr := <-c.workerErrCh:
			busyCount--
			key := fmt.Sprintf("%s/%s", jobErr.group, jobErr.name)
			existing := c.installations[key]
			existing.inProgress = false
			if existing.Equal(jobErr.installation) {
				existing.status = statusError
			}
			c.installations[key] = existing
			fmt.Printf("error getting %s/%s: %s", jobErr.group, jobErr.name, jobErr.err)
		case completedJob := <-c.workerDoneCh:
			busyCount--
			key := fmt.Sprintf("%s/%s", completedJob.group, completedJob.name)
			existing := c.installations[key]
			existing.inProgress = false
			if !existing.Equal(completedJob.installation) {
				c.installations[key] = existing
				break
			}
			switch completedJob.action {
			case actionInstall:
				existing.status = statusInstalled
				c.installations[key] = existing
			case actionRemove:
				delete(c.installations, key)
			}
		}
		if busyCount < workerCount {
			for key, item := range c.installations {
				if item.inProgress {
					continue
				}
				switch item.status {
				case statusInstallation:
					c.workerJobCh <- job{action: actionInstall, installation: item}
					busyCount++
					item.inProgress = true
					c.installations[key] = item
				case statusCleanup:
					c.workerJobCh <- job{action: actionRemove, installation: item}
					busyCount++
					item.inProgress = true
					c.installations[key] = item
				}
				if busyCount >= workerCount {
					break
				}
			}
		}
	}
}

func (c *Server) startWorker(jobCh <-chan job, doneCh chan<- job, errCh chan<- jobError) {
	for item := range jobCh {
		contentRoot := filepath.Join(c.ContentPath, item.group, item.name)
		switch item.action {
		case actionRemove:
			if err := os.RemoveAll(contentRoot); err != nil {
				errCh <- jobError{job: item, err: err}
				continue
			}
			doneCh <- item
		case actionInstall:
			blobs, err := driver.Retrieve(item.descriptor)
			if err != nil {
				errCh <- jobError{job: item, err: fmt.Errorf("could not retrieve content for %s/%s: %s", item.group, item.name, err)}
				continue
			}

			if err = c.storage.Install(item.name, item.group, blobs); err != nil {
				errCh <- jobError{job: item, err: fmt.Errorf("failed to install extracted content: %s", err)}
				continue
			}

			doneCh <- item
		}
	}
}

// Reload updates the installations according to the config data.
func (c *Server) Reload() error {
	visitedFunctions := map[string]struct{}{}
	functions, err := c.Repo.List()
	if err != nil {
		return fmt.Errorf("could not list functions from repo: %s", err)
	}
	for _, function := range functions {
		if len(function.Frontend.Source) == 0 {
			continue
		}
		key := fmt.Sprintf("%s/%s", function.Group, function.Name)
		item, ok := c.installations[key]
		descriptor, err := source.Parse(function.Frontend.Source)
		if err != nil {
			return fmt.Errorf("error parsing source %s for function %s: %s", function.Frontend.Source, key, err)
		}
		install := installation{
			name:       function.Name,
			group:      function.Group,
			descriptor: descriptor,
		}

		switch {
		case !ok:
			c.addCh <- install
		case ok && !item.descriptor.Equal(descriptor):
			c.updateCh <- install
		}
		visitedFunctions[key] = struct{}{}
	}

	for key := range c.installations {
		if _, ok := visitedFunctions[key]; !ok {
			// need to remove this function.
			c.removeCh <- key
		}
	}

	return nil
}

func (c *Server) Probe(context.Context, *ProbeRequest) (*ProbeResponse, error) {
	items := []*Content{}
	for _, item := range c.installations {
		var status Content_Status
		switch item.status {
		case statusInstalled:
			status = Content_READY
		case statusInstallation:
			if item.inProgress {
				status = Content_INSTALLING
			} else {
				status = Content_INSTALLATION_QUEUE
			}
		case statusCleanup:
			if item.inProgress {
				status = Content_CLEANING_UP
			} else {
				status = Content_CLEAN_UP_QUEUE
			}
		case statusError:
			status = Content_ERROR
		}
		items = append(items, &Content{
			Name:   item.name,
			Group:  item.group,
			Status: status,
		})
	}
	return &ProbeResponse{Items: items}, nil
}
func (c *Server) mustEmbedUnimplementedContentProbeServiceServer() {}
