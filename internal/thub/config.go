package thub

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/toolhub/toolhub/pkg/api"
)

// AuthConfig holds authentication information.
type AuthConfig struct {
	Token string `json:"token,omitempty"`
}

// RepositoryConfig holds the configurations related to a specific repository.
type RepositoryConfig struct {
	Name      string                         `json:"name,omitempty"`
	Auth      AuthConfig                     `json:"auth,omitempty"`
	URL       string                         `json:"url,omitempty"`
	Endpoints map[string]*api.AccessEndpoint `json:"endpoints,omitempty"`
}

// SanitizeURL prepares the URL for the RPC.
func (r RepositoryConfig) SanitizeURL() string {
	if strings.Contains(r.URL, ":") {
		return r.URL
	}
	return fmt.Sprintf("%s:80", r.URL)
}

// ClientConfig holds config structure for the client cli.
type ClientConfig struct {
	Repositories []RepositoryConfig `json:"repositories,omitempty"`
}

// Repository returns the repository with the registered name.
func (c ClientConfig) Repository(name string) (RepositoryConfig, bool) {
	for _, repo := range c.Repositories {
		if repo.Name == name {
			return repo, true
		}
	}
	return RepositoryConfig{}, false
}

// SaveRepository updates the related repository or adds it if it is missing.
func (c *ClientConfig) SaveRepository(repo RepositoryConfig) {
	targetIndex := -1
	for index, item := range c.Repositories {
		if repo.Name == item.Name {
			targetIndex = index
			break
		}
	}
	if targetIndex == -1 {
		c.Repositories = append(c.Repositories, repo)
	} else {
		c.Repositories[targetIndex] = repo
	}
}

func getConfigFile() (string, error) {
	configDir, err := os.UserConfigDir()
	if err != nil {
		return "", err
	}
	return filepath.Join(configDir, "toolhub", "config.json"), nil
}

// Load looks for the client file config.
func Load() (ClientConfig, error) {
	var config ClientConfig

	configFile, err := getConfigFile()
	if err != nil {
		return config, err
	}

	file, err := os.Open(configFile)
	if err != nil {
		return config, err
	}
	defer file.Close()

	err = json.NewDecoder(file).Decode(&config)
	if err != nil {
		return config, err
	}

	return config, nil
}

// Save writes to the config file the content of the specified client config.
func Save(config ClientConfig) error {
	configFile, err := getConfigFile()
	if err != nil {
		return err
	}

	dir, _ := filepath.Split(configFile)
	if err := os.MkdirAll(dir, 0744); err != nil {
		return err
	}

	file, err := os.Create(configFile)
	if err != nil {
		return err
	}
	defer file.Close()

	if err := json.NewEncoder(file).Encode(config); err != nil {
		return err
	}

	return nil
}
