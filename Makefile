IMAGE_PREFIX := toolhub
REGISTRY := registry.hub.docker.com
TAG := latest

COMPONENTS = api cds dispatcher thub light-server

CONTAINER_CLI=docker

.PHONY: generate
generate:
	protoc --go_out=. --go-grpc_out=. protocol/*

install:
	go build -o ~/go/bin/thub --ldflags='-s -w' cmd/thub/*

$(COMPONENTS):
	$(CONTAINER_CLI) build -f docker/$@/Dockerfile --target $@ -t $(REGISTRY)/$(IMAGE_PREFIX)/$@:$(TAG) .

build-images: $(COMPONENTS)

test:
	go test ./...
