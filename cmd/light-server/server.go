package main

import (
	"flag"
	"log"
	"net"
	"net/http"
	"os"

	"gitlab.com/toolhub/toolhub/pkg/api"
	"gitlab.com/toolhub/toolhub/pkg/cdp"
	"gitlab.com/toolhub/toolhub/pkg/dispatcher"
	"gitlab.com/toolhub/toolhub/pkg/watcher"
	"google.golang.org/grpc"
	"gopkg.in/yaml.v2"
)

type rpcEndpoint struct {
	HTTP    string `json:"http,omitempty" yaml:"http,omitempty"`
	GRPC    string `json:"grpc,omitempty" yaml:"grpc,omitempty"`
	Gateway string `json:"gateway,omitempty" yaml:"gateway,omitempty"`
}

func (r rpcEndpoint) ToAPI() *api.RPCEndpoint {
	return &api.RPCEndpoint{
		Http:    r.HTTP,
		Grpc:    r.GRPC,
		Gateway: r.Gateway,
	}
}

// accessEndpoint is only here to support yaml files.
type accessEndpoint struct {
	CDS        string      `json:"cds,omitempty" yaml:"cds,omitempty"`
	Dispatcher rpcEndpoint `json:"dispatcher,omitempty" yaml:"dispatcher,omitempty"`
	API        rpcEndpoint `json:"api,omitempty" yaml:"api,omitempty"`
	Dashboard  string      `json:"dashboard,omitempty" yaml:"dashboard,omitempty"`
	Insecure   bool        `json:"insecure,omitempty" yaml:"insecure,omitempty"`
}

func (a accessEndpoint) ToAPI() *api.AccessEndpoint {
	return &api.AccessEndpoint{
		Cds:        a.CDS,
		Dispatcher: a.Dispatcher.ToAPI(),
		Api:        a.API.ToAPI(),
		Insecure:   a.Insecure,
		Dashboard:  a.Dashboard,
	}
}

type apiConfig struct {
	Address   string                    `json:"address,omitempty" yaml:"address,omitempty"`
	Name      string                    `json:"name,omitempty" yaml:"name,omitempty"`
	Namespace string                    `json:"namespace,omitempty" yaml:"namespace,omitempty"`
	Endpoints map[string]accessEndpoint `json:"endpoints,omitempty"`
}

type cdsConfig struct {
	Address     string `json:"address,omitempty" yaml:"address,omitempty"`
	ContentPath string `json:"content_path,omitempty" yaml:"content_path,omitempty"`
}

type dispatcherConfig struct {
	Address string `json:"address,omitempty" yaml:"address,omitempty"`
}

type configData struct {
	FunctionsConfigPath string           `json:"functions_config_path,omitempty" yaml:"functions_config_path,omitempty"`
	API                 apiConfig        `json:"api,omitempty" yaml:"api,omitempty"`
	CDS                 *cdsConfig       `json:"cds,omitempty" yaml:"cds,omitempty"`
	Dispatcher          dispatcherConfig `json:"dispatcher,omitempty" yaml:"dispatcher,omitempty"`
}

func main() {
	configFile := flag.String("config", "", "if specified, a config file will get loaded.")

	flag.Parse()

	file, err := os.Open(*configFile)
	if err != nil {
		log.Fatalf("could not open config file: %s", err)
	}
	defer file.Close()
	var data configData
	if err := yaml.NewDecoder(file).Decode(&data); err != nil {
		log.Fatalf("failed to parse config file: %s", err)
	}

	cdsEnabled := data.CDS != nil
	configWatcher := watcher.NewFunctionsWatcher(data.FunctionsConfigPath)
	if err := configWatcher.Start(); err != nil {
		log.Fatalf("failed to start watching config file: %s", err)
	}

	dispatcher := dispatcher.NewServer(configWatcher.Repo())
	var contentServer *cdp.Server
	if cdsEnabled {
		cds, err := cdp.New(configWatcher.Repo(), data.CDS.ContentPath)
		if err != nil {
			log.Fatalf("failed to initialize content delivery service: %s", err)
		}
		contentServer = cds

		if err := contentServer.Reload(); err != nil {
			log.Fatalf("failed to load content delivery: %s", err)
		}
	}

	go func() {
		for {
			select {
			case <-configWatcher.Updates:
				if cdsEnabled {
					if err := contentServer.Reload(); err != nil {
						log.Printf("failed to sync content: %s", err)
					}
				}
			case err := <-configWatcher.Errors:
				log.Printf("failed to process config: %s", err)
			}
		}
	}()

	// dispatcher HTTP server.
	go func() {
		mux := http.NewServeMux()
		mux.Handle("/", dispatcher)
		log.Fatalln(http.ListenAndServe(data.Dispatcher.Address, mux))
	}()

	// CDS
	if cdsEnabled {
		go func() {
			listener, err := net.Listen("tcp", data.CDS.Address)
			if err != nil {
				log.Fatalf("failed to listen on %s: %s", data.CDS.Address, err)
			}

			server := grpc.NewServer()
			cdp.RegisterContentProbeServiceServer(server, contentServer)
			log.Fatalln(server.Serve(listener))
		}()
	}

	// API
	listener, err := net.Listen("tcp", data.API.Address)
	if err != nil {
		log.Fatalf("failed to listen on %s: %s", data.API.Address, err)
	}

	server := grpc.NewServer()
	apiService := api.NewFlexibleServer(configWatcher.Repo(), &data.CDS.Address)
	endpoints := map[string]*api.AccessEndpoint{}
	for hostname, endpoint := range data.API.Endpoints {
		endpoints[hostname] = endpoint.ToAPI()
	}
	apiService.Endpoints = endpoints
	if err != nil {
		log.Fatalf("failed to create the API service: %s", err)
	}
	api.RegisterFunctionServiceServer(server, apiService)
	log.Fatalln(server.Serve(listener))

	// TODO: use missing frontend instead of error.
	// TODO: use three different loggers for each component so it is clear what is going on with three prefixes.
}
