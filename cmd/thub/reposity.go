package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/toolhub/toolhub/internal/thub"
	"gitlab.com/toolhub/toolhub/pkg/api"
	"gitlab.com/toolhub/toolhub/pkg/endpoint"
	"google.golang.org/grpc"
)

var (
	token string
)

func mustGetRepoEvaluateURL(name string) string {
	configs, err := thub.Load()
	switch {
	case os.IsNotExist(err):
		configs = thub.ClientConfig{}
	case err != nil:
		log.Fatalf("error reading config file: %s", err)
	}

	repoConfig, ok := configs.Repository(name)
	if !ok {
		log.Fatalf("repository '%s' does not exist. consider adding it first.", name)
	}

	endpoints := mustGetEndpointsOrSync(name)
	hostname, result, ok := endpoint.Select(endpoints, endpoint.MatchAPI(repoConfig.URL, endpoint.GRPC))
	if !ok {
		log.Println("hint: consider syncing with the API to update access endpoints.")
		log.Fatalln("could not find a suitable access endpoint for this repository.")
	}
	return endpoint.CreateHTTP(hostname, result.Dispatcher.Http, result.Insecure)
}

// mustGetEndpointsOrSync returns latest access endpoints or syncs first if no access endpoint is defined.
func mustGetEndpointsOrSync(name string) map[string]*api.AccessEndpoint {
	configs, err := thub.Load()
	switch {
	case os.IsNotExist(err):
		configs = thub.ClientConfig{}
	case err != nil:
		log.Fatalf("error reading config file: %s", err)
	}

	repoConfig, ok := configs.Repository(name)
	if !ok {
		log.Fatalf("repository '%s' does not exist. consider adding it first.", name)
	}

	if len(repoConfig.Endpoints) > 0 {
		return repoConfig.Endpoints
	}

	return mustSyncEndpoints(name)
}

// mustSyncEndpoints takes a repository, force updates its endpoints and saves the result.
func mustSyncEndpoints(name string) map[string]*api.AccessEndpoint {
	configs, err := thub.Load()
	switch {
	case os.IsNotExist(err):
		configs = thub.ClientConfig{}
	case err != nil:
		log.Fatalf("error reading config file: %s", err)
	}

	repoConfig, ok := configs.Repository(name)
	if !ok {
		log.Fatalf("repository '%s' does not exist. consider adding it first.", name)
	}

	conn, err := grpc.Dial(repoConfig.SanitizeURL(), grpc.WithInsecure())
	if err != nil {
		log.Fatalf("failed to connect to %s: %s", repoConfig.URL, err)
	}

	client := api.NewFunctionServiceClient(conn)
	response, err := client.ListEndpoints(context.Background(), &api.ListEndpointRequest{})
	if err != nil {
		log.Fatalf("failed to get endpoints: %s", err)
	}

	repoConfig.Endpoints = response.Endpoints
	configs.SaveRepository(repoConfig)

	if err := thub.Save(configs); err != nil {
		log.Fatalf("failed to save configs: %s", err)
	}

	return response.Endpoints
}

func handleRepoList(cmd *cobra.Command, args []string) {
	configs, err := thub.Load()
	if err != nil && !os.IsNotExist(err) {
		log.Fatalf("could not read config file: %s", err)
	}

	log.Printf("%-30s\t%-30s\n", "NAME", "URL")
	for _, repo := range configs.Repositories {
		log.Printf("%-30s\t%-30s\n", repo.Name, repo.URL)
	}
}

func handleRepoRemove(cmd *cobra.Command, args []string) {
	name := args[0]
	configs, err := thub.Load()
	if err != nil && !os.IsNotExist(err) {
		log.Fatalf("could not read config file: %s", err)
	}

	index := -1
	for i, repo := range configs.Repositories {
		if repo.Name == name {
			index = i
			break
		}
	}

	if index == -1 {
		log.Fatalf("repository '%s' does not exist.\n", name)
	}

	configs.Repositories = append(configs.Repositories[0:index], configs.Repositories[index+1:]...)

	if err := thub.Save(configs); err != nil {
		log.Fatalf("could not save config: %s", err)
	}
}

func handleRepoSync(cmd *cobra.Command, args []string) {
	name := args[0]

	_ = mustSyncEndpoints(name)

	log.Printf("updated.")
}

func handleRepoShow(cmd *cobra.Command, args []string) {
	name := args[0]

	configs, err := thub.Load()
	switch {
	case os.IsNotExist(err):
		configs = thub.ClientConfig{}
	case err != nil:
		log.Fatalf("error reading config file: %s", err)
	}

	repoConfig, ok := configs.Repository(name)
	if !ok {
		log.Fatalf("repository '%s' does not exist. consider adding it first.", name)
	}

	log.Printf("URL: %s", repoConfig.URL)
	log.Printf("Access Endpoints:")
	for hostname, endpoint := range repoConfig.Endpoints {
		info := map[string]string{
			"Content":         endpoint.Cds,
			"Evaluate (HTTP)": endpoint.Dispatcher.Http,
			"API (gRPC)":      endpoint.Api.Grpc,
			"API (gRPC-Web)":  endpoint.Api.Gateway,
			"Dashboard":       endpoint.Dashboard,
			"Is Secure":       fmt.Sprintf("%v", !endpoint.Insecure),
		}
		log.Printf(" - %s", hostname)
		for key, value := range info {
			log.Printf("     %-16s: %s", key, value)
		}
	}
}

func handleRepoAdd(cmd *cobra.Command, args []string) {
	name := args[0]
	url := args[1]

	configs, err := thub.Load()
	switch {
	case os.IsNotExist(err):
		configs = thub.ClientConfig{}
	case err != nil:
		log.Fatalf("error reading config file: %s", err)
	}

	if _, ok := configs.Repository(name); ok {
		log.Fatalf("repository '%s' already exists.", name)
	}

	configs.Repositories = append(configs.Repositories, thub.RepositoryConfig{
		Name: name,
		URL:  url,
		Auth: thub.AuthConfig{
			Token: token,
		},
	})

	if err := thub.Save(configs); err != nil {
		log.Fatalf("could not save config: %s", err)
	}
}

func init() {
	repoCommand := &cobra.Command{
		Use: "repo",
	}

	addCmd := &cobra.Command{
		Use:   "add [name] [url]",
		Short: "add a new repository.",
		Args:  cobra.ExactArgs(2),
		Run:   handleRepoAdd,
	}
	addCmd.PersistentFlags().StringVar(&token, "token", "", "security token used for authentication")

	listCmd := &cobra.Command{
		Use:   "list",
		Short: "list all repositories.",
		Run:   handleRepoList,
	}

	removeCmd := &cobra.Command{
		Use:   "remove [name]",
		Short: "remove a repository",
		Args:  cobra.ExactArgs(1),
		Run:   handleRepoRemove,
	}

	syncCmd := &cobra.Command{
		Use:   "sync [name]",
		Short: "sync hostnames.",
		Args:  cobra.ExactArgs(1),
		Run:   handleRepoSync,
	}

	showCmd := &cobra.Command{
		Use:   "show [name]",
		Short: "show all information about this repository.",
		Args:  cobra.ExactArgs(1),
		Run:   handleRepoShow,
	}

	repoCommand.AddCommand(addCmd)
	repoCommand.AddCommand(listCmd)
	repoCommand.AddCommand(removeCmd)
	repoCommand.AddCommand(syncCmd)
	repoCommand.AddCommand(showCmd)

	rootCmd.AddCommand(repoCommand)
}
