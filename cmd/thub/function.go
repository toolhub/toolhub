package main

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"math"
	"net/http"
	"net/url"
	"os"
	"path"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/toolhub/toolhub/internal/thub"
	"gitlab.com/toolhub/toolhub/pkg/api"
	"gitlab.com/toolhub/toolhub/pkg/function"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"gopkg.in/yaml.v2"
)

var (
	funcName string
	repoName string
	fileName string
	rawData  string
	stdin    bool
)

func getConfig() thub.RepositoryConfig {
	configs, err := thub.Load()
	switch {
	case os.IsNotExist(err):
		log.Fatalln("no repository available, consider adding at least one repository.")
	case err != nil:
		log.Fatalf("error loading configs: %s", err)
	case len(repoName) > 0:
		repo, ok := configs.Repository(repoName)
		if ok {
			return repo
		}
		log.Fatalf("repository '%s' does not exist.", repoName)
	case len(configs.Repositories) > 0:
		return configs.Repositories[0]
	default:
		log.Fatalf("no repository available, consider adding at least one repository.")
	}

	return thub.RepositoryConfig{}
}

func handleListFuncs(cmd *cobra.Command, args []string) {
	repo := getConfig()

	connection, err := grpc.Dial(repo.SanitizeURL(), grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not dial %s: %s", repo.URL, err)
	}

	client := api.NewFunctionServiceClient(connection)
	response, err := client.ListFunctions(context.Background(), &api.ListFunctionRequest{})
	if err != nil {
		log.Fatalf("could not get list of functions: %s", err)
	}

	if len(response.Functions) == 0 {
		fmt.Println("No registered functions in this repository.")
		return
	}

	fmt.Printf("%-35s%-12s%-16s%-20s\n", "NAME", "STATUS", "DISPATCHER", "FRONT END")
	for _, item := range response.Functions {
		name := fmt.Sprintf("%s/%s", item.Group, item.Name)
		fmt.Printf("%-35s%-12s%-16s%-20s\n",
			name, item.Status.Status,
			strings.TrimPrefix(item.Status.BackendStatus.String(), "BACKEND_"),
			strings.TrimPrefix(item.Status.FrontendStatus.String(), "FRONTEND_"))
	}
}

func handleGetFunc(cmd *cobra.Command, args []string) {
	repo := getConfig()
	fullName := args[0]

	parts := strings.Split(fullName, "/")
	group := parts[0]
	name := parts[1]

	connection, err := grpc.Dial(repo.SanitizeURL(), grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not dial %s: %s", repo.URL, err)
	}

	client := api.NewFunctionServiceClient(connection)
	response, err := client.GetFunction(context.Background(), &api.GetFunctionRequest{
		Name:  name,
		Group: group,
	})
	if err != nil {
		log.Fatalf("could not get function: %s", err)
	}

	f := response.Function
	type Record struct {
		Header, Value string
	}
	backendStatus := strings.TrimPrefix(f.Status.BackendStatus.String(), "BACKEND_")
	frontendStatus := strings.TrimPrefix(f.Status.FrontendStatus.String(), "FRONTEND_")
	data := []Record{
		{"Group", f.Group},
		{"Name", f.Name},
		{"Status", fmt.Sprintf("%s (backend: %s, frontend: %s)", f.Status.Status.String(), backendStatus, frontendStatus)},
	}
	if f.Owner != nil {
		data = append(data, Record{"Owner", fmt.Sprintf("%s (id: %s)", f.Owner.Name, f.Owner.Id)})
	}
	if f.Metadata != nil {
		if ver := f.Metadata.Version; len(ver) > 0 {
			data = append(data, Record{"Version", ver})
		}
		if tags := f.Metadata.Tags; tags != nil {
			hashTags := make([]string, 0, len(tags))
			for _, item := range tags {
				hashTags = append(hashTags, "#"+item)
			}
			data = append(data, Record{"Tags", strings.Join(hashTags, " ")})
		}
		if desc := f.Metadata.Description; len(desc) > 0 {
			data = append(data, Record{"Description", desc})
		}
		if url := f.Metadata.SourceUrl; len(url) > 0 {
			data = append(data, Record{"Source URL", url})
		}
	}
	var max float64 = 0
	for _, record := range data {
		max = math.Max(max, float64(len(record.Header)))
	}
	format := "%" + strconv.Itoa(int(max)) + "s :  %s\n"
	for _, record := range data {
		fmt.Printf(format, record.Header, record.Value)
	}
}

func handleEvaluateFunc(cmd *cobra.Command, args []string) {
	name := args[0]
	repo := getConfig()

	genericURL := mustGetRepoEvaluateURL(repo.Name)
	url, err := url.Parse(genericURL)
	if err != nil {
		log.Fatalf("failed to parse access endpoint URL '%s': %s", genericURL, err)
	}
	url.Path = path.Join(url.Path, name, "evaluate") + "/"

	var input io.Reader
	if stdin {
		input = os.Stdin
	} else if len(fileName) > 0 {
		file, err := os.Open(fileName)
		if err != nil {
			log.Fatalln(err)
		}
		defer file.Close()
		input = file
	} else if len(rawData) > 0 {
		input = strings.NewReader(rawData)
	} else {
		input = &bytes.Buffer{}
	}

	response, err := http.Post(url.String(), "application/octet-stream", input)
	if err != nil {
		log.Fatalf("failed: %s", err)
	}
	if response.StatusCode != 200 {
		fmt.Printf("Status: %s", response.Status)
	}
	if response.Body != nil {
		defer response.Body.Close()
		io.Copy(os.Stdout, response.Body)
	}
}

func handleShowFuncURL(cmd *cobra.Command, args []string) {
	name := args[0]
	repo := getConfig()

	genericURL := mustGetRepoEvaluateURL(repo.Name)
	url, err := url.Parse(genericURL)
	if err != nil {
		log.Fatalf("failed to parse access endpoint URL '%s': %s", genericURL, err)
	}
	url.Path = path.Join(url.Path, name, "evaluate") + "/"
	log.Println(url.String())
}

func handleCreateFunc(cmd *cobra.Command, args []string) {
	repo := getConfig()

	connection, err := grpc.Dial(repo.SanitizeURL(), grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not dial %s: %s", repo.URL, err)
	}

	client := api.NewFunctionServiceClient(connection)
	ctx := context.Background()
	if len(repo.Auth.Token) > 0 {
		ctx = metadata.AppendToOutgoingContext(ctx, "authorization", repo.Auth.Token)
	}
	request, err := createRequestFromFile(fileName)
	if err != nil {
		log.Fatalf("failed to parse function descriptor file: %s", err)
	}

	if len(funcName) > 0 {
		parts := strings.Split(funcName, "/")
		if len(parts) != 2 {
			log.Fatalf("invalid function name: %s", funcName)
		}
		if parts[0] != "*" {
			request.Group = parts[0]
		}
		if parts[1] != "*" {
			request.Name = parts[1]
		}
	}

	if len(request.Name) == 0 {
		log.Fatalln("invalid function: name must be provided.")
	}

	if len(request.Group) == 0 {
		log.Fatalln("invalid function: group must be provided.")
	}

	_, err = client.CreateFunction(ctx, request)
	if err != nil {
		log.Fatalf("failed to create function in the repo: %s", err)
	}
	log.Printf("done.")
}

func createRequestFromFile(path string) (*api.CreateFunctionRequest, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := file.Close(); err != nil {
			log.Printf("failed to close %s: %s", path, err)
		}
	}()

	descriptor := function.Descriptor{}
	if err := yaml.NewDecoder(file).Decode(&descriptor); err != nil {
		return nil, fmt.Errorf("failed to read yaml file '%s': %s", path, err)
	}

	if len(descriptor.Backend.Image) == 0 {
		if len(descriptor.Backend.Endpoint) > 0 {
			return nil, errors.New("function reverse proxy cannot be created via API at this time")
		}
		return nil, errors.New("image must be provided for the function")
	}

	backendSpec := &api.BackendSpec{
		Image: descriptor.Backend.Image,
		Type:  descriptor.Backend.Type,
		Port:  4000,
	}
	if port := descriptor.Backend.Port; port != nil {
		backendSpec.Port = *port
	}

	var frontndSpec *api.FrontendSpec
	if frontend := descriptor.Frontend; frontend != nil {
		frontndSpec = &api.FrontendSpec{
			Source: frontend.Source,
			Type:   frontend.Type,
		}
	}

	metadata := &api.Metadata{
		Version: descriptor.Version,
	}
	if meta := descriptor.Meta; meta != nil {
		metadata.Description = meta.Description
		metadata.Tags = meta.Tags
		metadata.SourceUrl = meta.SourceURL
	}

	request := &api.CreateFunctionRequest{
		Group:    descriptor.Group,
		Name:     descriptor.Name,
		Backend:  backendSpec,
		Frontend: frontndSpec,
		Metadata: metadata,
	}

	if owner := descriptor.Owner; owner != nil {
		request.Owner = &api.Account{
			Id:   owner.ID,
			Name: owner.Name,
		}
	}
	return request, nil
}

func init() {
	functionCommand := &cobra.Command{
		Use:   "func",
		Short: "manage functions in a repository.",
	}

	listCmd := &cobra.Command{
		Use:   "list",
		Short: "list all functions in a repository.",
		Run:   handleListFuncs,
	}
	listCmd.PersistentFlags().StringVar(&repoName, "repo", "", "the repository to use.")

	getCmd := &cobra.Command{
		Use:   "get [name]",
		Short: "get status of a specific function by name.",
		Args:  cobra.ExactArgs(1),
		Run:   handleGetFunc,
	}
	getCmd.PersistentFlags().StringVar(&repoName, "repo", "", "the repository to use.")

	evaluateCmd := &cobra.Command{
		Use:   "evaluate [name]",
		Short: "evaluate a function by directing stdin or",
		Args:  cobra.ExactArgs(1),
		Run:   handleEvaluateFunc,
	}
	evaluateCmd.PersistentFlags().StringVar(&repoName, "repo", "", "the repository to use.")
	evaluateCmd.PersistentFlags().StringVar(&fileName, "file", "", "the file to open for input.")
	evaluateCmd.PersistentFlags().BoolVar(&stdin, "stdin", false, "whether or not to take value from stdin.")
	evaluateCmd.PersistentFlags().StringVar(&rawData, "data", "", "whether or not to take value from stdin.")

	showFuncCmd := &cobra.Command{
		Use:   "get-evaluate-url [name]",
		Short: "get an HTTP url for calling the function.",
		Args:  cobra.ExactArgs(1),
		Run:   handleShowFuncURL,
	}
	showFuncCmd.PersistentFlags().StringVar(&repoName, "repo", "", "the repository to use.")

	createFuncCmd := &cobra.Command{
		Use:   "create",
		Short: "create function adds a new function and admits to the repository.",
		Run:   handleCreateFunc,
	}
	createFuncCmd.PersistentFlags().StringVar(&repoName, "repo", "", "the repository to use.")
	createFuncCmd.PersistentFlags().StringVar(&fileName, "file", "", "the file to open for input.")
	createFuncCmd.PersistentFlags().StringVar(
		&funcName, "name", "", "override group or name with this flag. */myname, mygroup/* and myname/mygroup are valid.")

	functionCommand.AddCommand(listCmd)
	functionCommand.AddCommand(getCmd)
	functionCommand.AddCommand(evaluateCmd)
	functionCommand.AddCommand(showFuncCmd)
	functionCommand.AddCommand(createFuncCmd)

	rootCmd.AddCommand(functionCommand)
}
