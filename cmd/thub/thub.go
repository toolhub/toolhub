package main

import (
	"log"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "toolhub",
	Short: "A command line tool for tool hub repositories.",
}

func Execute() error {
	return rootCmd.Execute()
}

func main() {
	log.SetFlags(0)
	if err := Execute(); err != nil {
		log.Fatalln(err)
	}
}
