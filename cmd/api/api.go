package main

import (
	"encoding/json"
	"flag"
	"log"
	"net"
	"net/http"
	"os"

	"github.com/improbable-eng/grpc-web/go/grpcweb"
	"gitlab.com/toolhub/toolhub/pkg/api"
	"gitlab.com/toolhub/toolhub/pkg/endpoint"
	"gitlab.com/toolhub/toolhub/pkg/httprouter"
	"gitlab.com/toolhub/toolhub/pkg/watcher"
	"google.golang.org/grpc"
)

func mustLoadEndpoints(svc *api.FlexibleServer, configPath string) {
	file, err := os.Open(configPath)
	if err != nil {
		log.Fatalf("failed to load '%s': %s", configPath, err)
	}
	defer file.Close()
	var config endpoint.Config
	if err := json.NewDecoder(file).Decode(&config); err != nil {
		log.Fatalf("failed to decode config file: %s", err)
	}
	svc.Endpoints = config.Endpoints
}

func corsAllowAll(string) bool {
	return true
}

func main() {
	var address, grpcWebAddress string
	var cdsURL string
	var endpointsConfig, config string
	var delegateAPI, delegateToken string

	flag.StringVar(&address, "address", ":7000", "the address on which to serve.")
	flag.StringVar(&grpcWebAddress, "grpc-web-address", ":7001", "the address on which to serve grpc web http traffic.")
	flag.StringVar(&cdsURL, "cds", "", "URL to the CDS service (optional)")
	flag.StringVar(&endpointsConfig, "endpoints-config", "", "path to the config file that holds all endpoints.")
	flag.StringVar(&config, "config", "", "path to the config file that holds all functions.")
	flag.StringVar(&delegateAPI, "delegate-api", "", "address of the delegate API to use for creating functions.")

	delegateToken = os.Getenv("TOOLSTUDIO_DELEGATE_TOKEN")
	primaryToken := os.Getenv("TOOLSTUDIO_PRIMARY_TOKEN")

	flag.Parse()

	listener, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("could not listen on %s: %s", address, err)
	}

	// setup config watcher.
	configWatcher := watcher.NewFunctionsWatcher(config)
	if err := configWatcher.Start(); err != nil {
		log.Fatalf("error watching config file '%s': %s", config, err)
	}
	go func() {
		for {
			select {
			case <-configWatcher.Updates:
			case err := <-configWatcher.Errors:
				log.Printf("error loading config: %s", err)
			}
		}
	}()

	var cds *string
	if len(cdsURL) > 0 {
		cds = &cdsURL
	}
	svc := api.NewFlexibleServer(configWatcher.Repo(), cds)
	svc.PrimaryToken = primaryToken

	if len(delegateAPI) > 0 {
		svc.Writer = api.NewDelegateRepo(delegateAPI, delegateToken)
	}

	// watch the endpoints config file.
	if len(endpointsConfig) > 0 {
		configWatcher := watcher.NewFileWatcher(endpointsConfig)
		if err := configWatcher.Start(); err != nil {
			log.Fatalf("could not watch '%s': %s", endpointsConfig, err)
		}
		go func() {
			mustLoadEndpoints(svc, endpointsConfig)
			for {
				select {
				case <-configWatcher.Events:
					mustLoadEndpoints(svc, endpointsConfig)
				case err := <-configWatcher.Errors:
					log.Printf("failed to watch config file: %s", err)
				}
			}
		}()
	}

	server := grpc.NewServer()

	go func() {
		api.RegisterFunctionServiceServer(server, svc)
		log.Fatalln(server.Serve(listener))
	}()

	options := []grpcweb.Option{grpcweb.WithOriginFunc(corsAllowAll)}
	wrappedGrpc := grpcweb.WrapServer(server, options...)

	var handler http.Handler

	if len(endpointsConfig) > 0 {
		errorCh := make(chan error)
		routesCh := make(chan []string)
		go func() {
			for {
				select {
				case routes := <-routesCh:
					log.Printf("handling routes %v", routes)
				case err := <-errorCh:
					log.Printf("multiplexer error: %s", err)
				}
			}
		}()
		mux, err := httprouter.New(httprouter.WithConfigWatching(wrappedGrpc, endpointsConfig, httprouter.API, errorCh, routesCh))
		if err != nil {
			log.Fatalf("failed to initialize multiplexer: %s", err)
		}
		handler = mux
	} else {
		mux := http.NewServeMux()
		mux.Handle("/", wrappedGrpc)
		handler = mux
	}
	log.Fatalln(http.ListenAndServe(grpcWebAddress, handler))
}
