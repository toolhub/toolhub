package main

import (
	"flag"
	"log"
	"net/http"

	"github.com/rs/cors"

	"gitlab.com/toolhub/toolhub/pkg/dispatcher"
	"gitlab.com/toolhub/toolhub/pkg/httprouter"
	"gitlab.com/toolhub/toolhub/pkg/watcher"
)

func main() {
	var configPath, accessEndpointsPath string
	var address string

	flag.StringVar(&configPath, "config", "", "The path of the config file.")
	flag.StringVar(&address, "addr", ":4000", "The address to listen on.")
	flag.StringVar(&accessEndpointsPath, "endpoints-config", "", "(optional) path to the access endpoints config.")

	flag.Parse()
	log.SetFlags(0)

	// setup config watcher.
	configWatcher := watcher.NewFunctionsWatcher(configPath)
	if err := configWatcher.Start(); err != nil {
		log.Fatalf("error watching config file '%s': %s", configPath, err)
	}
	go func() {
		for {
			select {
			case <-configWatcher.Updates:
			case err := <-configWatcher.Errors:
				log.Printf("error loading config: %s", err)
			}
		}
	}()

	// Allow any origin so long as they ask for POST or HEAD only.
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{
			http.MethodHead,
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
		},
		AllowedHeaders:   []string{"*"},
		ExposedHeaders:   []string{"*"},
		AllowCredentials: false,
		MaxAge:           60 * 60,
	})

	server := dispatcher.NewServer(configWatcher.Repo())

	var handler http.Handler

	if len(accessEndpointsPath) > 0 {
		// Use the default mux by default.
		errorCh := make(chan error)
		routeCh := make(chan []string)
		go func() {
			for {
				select {
				case routes := <-routeCh:
					log.Printf("handling routes %v", routes)
				case err := <-errorCh:
					log.Printf("multiplexer error: %s", err)
				}
			}
		}()

		options := []httprouter.Option{
			httprouter.WithConfigWatching(server, accessEndpointsPath, httprouter.Dispatcher, errorCh, routeCh),
		}
		router, err := httprouter.New(options...)
		if err != nil {
			log.Fatalf("failed to initialize multiplexer: %s", err)
		}
		handler = router
	} else {
		mux := http.NewServeMux()
		mux.Handle("/", server)
		handler = mux
	}

	log.Fatalln(http.ListenAndServe(address, c.Handler(handler)))
}
