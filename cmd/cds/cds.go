package main

import (
	"flag"
	"log"
	"net"

	"gitlab.com/toolhub/toolhub/pkg/cdp"
	"gitlab.com/toolhub/toolhub/pkg/watcher"
	"google.golang.org/grpc"
)

func main() {
	var address string
	var contentPath, configPath string
	flag.StringVar(&address, "address", "0.0.0.0:5000", "address to listen on.")
	flag.StringVar(&contentPath, "dir", "", "where to store the files.")
	flag.StringVar(&configPath, "config", "", "the functions config.")
	flag.Parse()

	configWatcher := watcher.NewFunctionsWatcher(configPath)
	if err := configWatcher.Start(); err != nil {
		log.Fatalf("failed to watch config file %s: %s", configPath, err)
	}

	listener, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("failed to listen on %s: %s", address, err)
	}

	svc, err := cdp.New(configWatcher.Repo(), contentPath)
	if err != nil {
		log.Fatalf("could not init content delivery service: %s", err)
	}

	if err := svc.Reload(); err != nil {
		log.Fatalln(err)
	}

	go func() {
		for {
			select {
			case <-configWatcher.Updates:
				if err := svc.Reload(); err != nil {
					log.Fatalln(err)
				}
			case err := <-configWatcher.Errors:
				log.Printf("failed to load config file: %s", err)
			}
		}
	}()

	server := grpc.NewServer()
	cdp.RegisterContentProbeServiceServer(server, svc)
	log.Fatalln(server.Serve(listener))
}
